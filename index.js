const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

mongoose
  .connect(
    "mongodb+srv://eclespinosa:emilmongo@cluster0.vtyel.mongodb.net/b125-tasks?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log("Connected to Database");
  })
  .catch((error) => {
    console.log(error);
  });

/* To do list */
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: Boolean,
    default: false,
  },
});
const Task = mongoose.model("Task", taskSchema);

/* User */
const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  userName: String,
  password: String,
});
const User = mongoose.model("User", userSchema);

// CREATE in CRUD
app.post("/add-task", (req, res) => {
  let newTask = new Task({
    name: req.body.name,
  });

  newTask.save((err, savedTask) => {
    if (err) {
      console.log(err);
    } else {
      res.send(`New task saved, ${savedTask}`);
    }
  });
});

app.post("/register", (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    userName: req.body.userName,
    password: req.body.password,
  });

  newUser.save((err, registeredUser) => {
    if (err) {
      console.log(err);
    } else {
      res.send(`Registered Successfully, ${registeredUser}`);
    }
  });
});

// RETRIEVE in CRUD
app.get("/retrieve-tasks", (req, res) => {
  Task.find({}, (error, records) => {
    if (error) {
      console.log(error);
    } else {
      res.send(records);
    }
  });
});

app.get("/retrieve-tasks-done", (req, res) => {
  // The task model will return all the tasks that has a status equal to true
  Task.find({ status: true }, (error, records) => {
    if (error) {
      console.log(error);
    } else {
      res.send(records);
    }
  });
});

// UPDATE in CRUD
app.put("/complete-task/:taskId", (req, res) => {
  const taskId = req.params.taskId;
  Task.findByIdAndUpdate(taskId, { status: true }, (error, updatedTask) => {
    if (error) {
      console.log(error);
    } else {
      res.send(`Task Completed, ${updatedTask}`);
    }
  });
});

// DELETE in CRUD
app.delete("/delete-task/:taskId", (req, res) => {
  // findByIdAndDelete() - find a specific record using its ID and Delete it
  let { taskId } = req.params;
  Task.findByIdAndDelete(taskId, (error, deletedTask) => {
    if (error) {
      console.log(error);
    } else {
      res.send(`Task Deleted, ${deletedTask}`);
    }
  });
});

app.listen(PORT, () => console.log(`Server is running at port ${PORT}`));
